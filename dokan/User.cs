﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//storing users login info
namespace dokan
{
    public class User
    {
        private String EncryptedPassword = String.Empty;
        public User(String fname, String lname, String dob, String email, String contactno, String password)
        {
            FirstName = fname;
            LastName = lname;
            DOB = dob;
            Email = email;
            ContactNo = contactno;
            Password = password;
        }
        public User(String name, String password)
        {
            Name = name;
            Password = password;
        }
        public String Name { get; set; }
        public String Password
        {
            get
            {
                return EncryptedPassword;
            }
            set
            {
                EncryptedPassword = value;
            }
        }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String DOB { get; set; }
        public String Email { get; set; }
        public String ContactNo { get; set; }
    }
}
