﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace dokan
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class loginPage : Page
    {
        //LoginChecker object
        private LoginChecker LC;
        public loginPage()
        {
            this.InitializeComponent();
            LC = new LoginChecker();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            //LoginChecker lc = new LoginChecker();
            //SignUp.Content = Password.Password.ToString();
            
            bool checker = false;
            checker = LC.CheckLogin(UserID.Text.ToString(), Password.Password.ToString());
            if (checker == true)
            {
                ShowMessageLoginSuccessful();
            }
            else
            {
                ShowMessageLoginDenied();
            }
            UserID.Text = String.Empty;
            Password.Password = String.Empty;
             
        }
        //Show message dialog
        private async void ShowMessageLoginSuccessful()
        {
            await new MessageDialog("Login Successful!!!").ShowAsync();
        }
        private async void ShowMessageLoginDenied()
        {
            await new MessageDialog("Login Denied\nPlease give valid username and password").ShowAsync();
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Registration));
        }
    }
}
