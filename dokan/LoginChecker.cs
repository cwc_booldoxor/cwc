﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Have or fetch all the login data from DB and check validity return true/false
namespace dokan
{
    public class LoginChecker
    {
        //dummy user
        List<User> UserList;
        //initialize users
        public LoginChecker()
        {
            UserList = new List<User>();
            UserList.Add(new User("Samiul", "00001"));
            UserList.Add(new User("Sanjary", "00002"));
            UserList.Add(new User("Asif", "00003"));
            UserList.Add(new User("Fahme", "00004"));
            UserList.Add(new User("Faiyaz", "00005"));
            
        }
        //check login info correct or not, returns boolean
        public bool CheckLogin(String uname, String passwrd)
        {
            for (int i = 0; i < UserList.Count; i++)
            {
                if(UserList[i].Name == uname)
                {
                    if (UserList[i].Password == passwrd)
                    {
                        return true;
                    }
                    else return false;
                }
            }
            return false;
        }
    }
}
