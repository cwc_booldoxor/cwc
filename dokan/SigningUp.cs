﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dokan
{
    public class SigningUp
    {
        //dummy user
        List<User> UserList;

        public SigningUp()
        {
            UserList = new List<User>();
        }
        public bool SignUp(String fname, String lname, String dob, String email, String contactno, String password)
        {
            UserList.Add(new User(fname,lname,dob,email,contactno,password));
            return true;
        }
    }
}
