﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace dokan
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Registration : Page
    {
        private SigningUp SignUpObj;
        public Registration()
        {
            this.InitializeComponent();
            SignUpObj = new SigningUp();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text.ToString() != String.Empty && LastName.Text.ToString() != String.Empty && DOB.DataContext.ToString() != String.Empty && Email.Text.ToString() != String.Empty && ContactNo.Text.ToString() != String.Empty && NewPassword.Password.ToString() != String.Empty && RetypePassword.Password.ToString() != String.Empty && NewPassword.Password == RetypePassword.Password)
            {
                //*signing up
                if(SigningUp() == true)
                {
                    SignupSuccessful();
                }
                else
                {
                    SignupFailed();
                }
            }
            else if (NewPassword.Password != RetypePassword.Password)
            {
                PasswordMissmatchMessage();
            }
            else
            {
                AllFieldRequired();
            }
        }
        //show invalidation message
        private async void PasswordMissmatchMessage()
        {
            await new MessageDialog("Sorry your given passwords did not match").ShowAsync();
        }
        private async void AllFieldRequired()
        {
            await new MessageDialog("Please provide all informations").ShowAsync();
        }
        private async void SignupSuccessful()
        {
            await new MessageDialog("Signup Successful").ShowAsync();
        }
        private async void SignupFailed()
        {
            await new MessageDialog("Signup Failed").ShowAsync();
        }
        //creating profile, storing signup info
        private bool SigningUp()
        {
            bool ret = SignUpObj.SignUp(FirstName.Text.ToString(), LastName.Text.ToString(), DOB.DataContext.ToString(), Email.Text.ToString(), ContactNo.Text.ToString(), NewPassword.Password.ToString());
            return ret;
        }
    }
}
